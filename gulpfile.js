const dotenv = require('dotenv');
dotenv.config();

const { series, parallel, src, dest, watch } = require('gulp');
const sass = require('gulp-sass');
const concat = require("gulp-concat");
const purgecss = require('gulp-purgecss');
const autoprefixer = require('autoprefixer');
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');

let theme_path = './public_html/wp-content/themes/' + process.env.THEME_SLUG;
let sass_src = [
    './development/sass/master.scss'
];
let editor_sass_src = [
    './development/sass/editor.scss'
];
let login_screen_src = [
    './development/sass/login-screen.scss'
];
let js_src = [
    './development/js/master.js',
];
let vendor_src = [
    // './node_modules/slick-carousel/slick/slick.min.js',
    // './node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',

    // './node_modules/bootstrap/js/dist/dom/selector-engine.js',
    // './node_modules/bootstrap/js/dist/dom/data.js',
    // './node_modules/bootstrap/js/dist/dom/event-handler.js',
    // './node_modules/bootstrap/js/dist/dom/manipulator.js',
    // './node_modules/bootstrap/js/dist/base-component.js',
    // './node_modules/bootstrap/js/dist/collapse.js',
];
let development_assets_src = [
    './development/**/*.*',
    '!./development/**/*.js',
    '!./development/**/*.scss',
    '!./development/**/*.txt',
    '!./development/**/*demo*',

];

// Sync WordPress files 
function wordpress_files() {
    return src([
        './wordpress/themes/**/*.*',
        './wordpress/mu-plugins/**/*.*',
        './wordpress/plugins/**/*.*',
        './wordpress/*.*',
    ], { base: './wordpress/' })
        .pipe(dest(
            './public_html/wp-content/'
        ));
}
function wordpress_config_files() {
    return src([
        './wordpress/*.*',
        './wordpress/.htaccess',
    ], { base: './wordpress/', allowEmpty: true })
        .pipe(dest(
            './public_html/'
        ));
}

// Development CSS
function development_css() {
    return src(sass_src)
        .pipe(sass())
        .pipe(postcss([autoprefixer()]))
        .pipe(concat('site.css'))
        .pipe(dest(
            theme_path + '/static/'
        ));
}

function editor_css() {
    return src(editor_sass_src)
        .pipe(sass())
        .pipe(postcss([ autoprefixer() ]))
        .pipe(concat('editor.css'))
        .pipe(dest(
            theme_path + '/static/'
        ));
}

function login_screen_css() {
    return src(login_screen_src)
        .pipe(sass())
        .pipe(postcss([ autoprefixer() ]))
        .pipe(concat('password-protected-login.css'))
        .pipe(dest(
            theme_path
        ));
}

// Development JS
function development_javascript() {
    return src(js_src)
        //.pipe(concat('site.min.js'))
        .pipe(dest(
            theme_path + '/static/'
        ));
}

// [Optional]: Copy over any compiled developments assets from starter theme
function development_assets() {
    return src(development_assets_src,  {base: './development/'})
        .pipe(dest(
            theme_path + '/static/dev'
        ));
}

// Production CSS - Bundle + Minify + Purge
function production_javascript() {
    return src(js_src)
        //.pipe(concat('site.min.js'))
        .pipe(uglify())
        .pipe(dest(
            theme_path + '/static/'
        ));
}

// Production JS - Bundle + Minify JS
function production_css() {
    return src(sass_src)
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(sourcemaps.init())
        .pipe(postcss([autoprefixer()]))
        .pipe(purgecss({
            content: [
                theme_path + '/templates/**/*.twig'
            ]
        }))
        .pipe(concat('site.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(dest(
            theme_path + '/static/'
        ));
}

function vendor_assets() {
    return src(vendor_src)
        .pipe(concat('vendor.js'))
        .pipe(dest(
            theme_path + '/static/'
        ));
}

if (process.env.SITE_ENV != 'local') {
    exports.build = series(parallel(wordpress_files, wordpress_config_files, development_assets, production_css, production_javascript, /* vendor_assets ,*/ login_screen_css));

} else {
    exports.default = function () {
        watch([
            './wordpress/themes/**/*.*',
            './wordpress/mu-plugins/**/*.*',
            './wordpress/plugins/**/*.*',
        ], wordpress_files);
        watch([
            './wordpress/*',
        ], wordpress_config_files);
        watch('./development/**/*.scss', development_css);
        watch('./development/**/*.js', javascript);
    };

    exports.build = series(parallel(wordpress_files, wordpress_config_files, development_assets, development_css, editor_css, development_javascript, /* vendor_assets ,*/ login_screen_css));
    exports.production = series(parallel(wordpress_files, wordpress_config_files, production_css, editor_css, production_javascript, /* vendor_assets */));
}