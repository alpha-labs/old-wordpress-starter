try {
    $StartTime = $(get-date)
    "{0:MM/dd/yy} {0:HH:mm:ss} - BEGIN" -f (Get-Date);

    # Load environment variables
    Set-DotEnv -path "../../.env";

    # Ensure all the WPSDB plugins aare active using WP-CLI first
    wp plugin activate wp-sync-db wp-sync-db-cli wp-sync-db-media-files --path=../../$env:ROOT_FOLDER

    "{0:MM/dd/yy} {0:HH:mm:ss} - Migration started ..." -f (Get-Date);

    # Clone the parent theme from the git repo
    wp --path=../../$env:ROOT_FOLDER wpsdb migrate --connection-info=$env:WPSDB_CONNECTION_INFO --action=pull --create-backup=1

    "{0:MM/dd/yy} {0:HH:mm:ss} - Script complete" -f (Get-Date);
} 
catch {
    "{0:MM/dd/yy} {0:HH:mm:ss} - Error: An error occurred that could not be resolved." -f (Get-Date);
}
finally {
    # Remove local environment variables
    Remove-DotEnv

    "{0:MM/dd/yy} {0:HH:mm:ss} - END" -f (Get-Date);
    
    # Show how long
    $elapsedTime = $(get-date) - $StartTime
    "Time to execute: {0:HH:mm:ss}" -f ([datetime]$elapsedTime.Ticks)
}