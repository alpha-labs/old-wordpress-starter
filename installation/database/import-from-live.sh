#!/usr/bin/env bash

SECONDS=0
echo "$(date  '+%Y-%m-%d %H:%M:%S') - BEGIN"

# # Load environment variables
set -o allexport;
source "../../.env"
set +o allexport

# # Ensure all the WPSDB plugins aare active using WP-CLI first
wp plugin activate wp-sync-db wp-sync-db-cli wp-sync-db-media-files --path="../../$ROOT_FOLDER"

echo "$(date  '+%Y-%m-%d %H:%M:%S') - Migration started ..."

# # Clone the parent theme from the git repo
wp --path="../../$ROOT_FOLDER" wpsdb migrate --connection-info=$WPSDB_CONNECTION_INFO --action=pull --create-backup=1


echo "$(date  '+%Y-%m-%d %H:%M:%S') - Script complete"

echo "Time to execute: $SECONDS"