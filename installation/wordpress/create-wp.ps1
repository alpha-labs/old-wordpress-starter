# Load environment variables
Set-DotEnv -path "../../.env";

# Create a WP database using the wp-config/env settings
wp core install --url=$env:SITE_DOMAIN --title=Something --admin_user=$env:ADMIN_NAME --admin_email=$env:ADMIN_EMAIL --path=../../$env:ROOT_FOLDER

# Remove local environment variables
Remove-DotEnv