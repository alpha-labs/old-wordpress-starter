# Load environment variables
set -o allexport;
source "../../.env"
set +o allexport

# Create a WP database using the wp-config/env settings
wp core install --url=$SITE_DOMAIN --title=Something --admin_user=$ADMIN_NAME --admin_email=$ADMIN_EMAIL --path=../../$ROOT_FOLDER
