# Load environment variables
Set-DotEnv -path "../../.env";

# Clone the parent theme from the git repo
git clone https://github.com/timber/starter-theme "../../wordpress/themes/$env:THEME_SLUG"

# Remove local environment variables
Remove-DotEnv