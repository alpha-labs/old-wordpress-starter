# Load environment variables
set -o allexport;
source "../../.env"
set +o allexport

# Clone the parent theme from the git repo
git clone https://github.com/timber/starter-theme "../../wordpress/themes/$THEME_SLUG"