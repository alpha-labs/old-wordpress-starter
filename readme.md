Original source: [https://bitbucket.org/alpha-labs/wordpress-starter](https://bitbucket.org/alpha-labs/wordpress-starter)

## Disclaimer

I created this WordPress project setup to be a useful way in organising projects, stream lining processes for source control and deployment, and allowing easier process to be applied to overall ongoing development. You are free to use this project structure at your own discression, but I am not planning to offer any support in the running or maintaining of projects created using it. I am also not responsibile for issues that arrise from the use of this structure or the code provided as a starting point. 

This is a work in progress and the following items are in progress:

- [ ] Write shell script versions of the scripts
- [ ] Remove PowerShell versions of the scripts as the dependencies are too complicated
- [ ] Write some guidance on setting up Deploy HQ
- [ ] Write some guidance on using Sentry
- [ ] Write some guidance on supporting tools and code

## Contents:
1. [Prerequisites](#markdown-header-prerequisits)
1. [First time setup / Installation](markdown-header-first-time-setup--installation)
    1. [Environment file](#markdown-header-environment-file)
    1. [Composer, NPM & Gulp](#markdown-header-composer-npm-gulp)
    1. [Quick setup](#markdown-header-quick-setup)
    1. [Manual setup](#markdown-header-manual-setup)
1. [New projects](#markdown-header-new-projects)
    1. [New builds](#markdown-header-new-builds)
    1. [Converting existing project to this structure](#markdown-header-converting-existing-project-to-this-structure)
1. [Development](#markdown-header-development)
    1. [Folder structure](#markdown-header-folder-structure)
    1. [Maintenance](#markdown-header-maintenance)
1. [Deployment](#markdown-header-deployment)

Prerequisits
===

This setup requires your machine and hosting environment to have the following capabilities available:

1. [Composer](https://getcomposer.org/)
1. [NPM](https://www.npmjs.com/)
1. [Gulp](https://gulpjs.com/)

To run the NPM tasks mentioned below, you will need either;

Windows - PowerShell (.ps1 files)

1. [PowerShell](https://en.wikipedia.org/wiki/PowerShell)
1. [DotEnv Extension](https://www.powershellgallery.com/packages/dotenv/0.1.0)

Other - Unix Shell (.sh files)

1. [Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell))

Optionally, the setup is best used with the following services:

1. Deployment - [DeployHQ](https://www.deployhq.com/)
1. Hosting - [SiteGround](https://www.siteground.co.uk/)

The [Quick setup](#markdown-header-quick-setup) instructions require you to be able to run Bash commands from your machine. For Windows users you can install Bash on your machine by enabling the **Windows Subsystem for Linux** component and installing the **Ubuntu** app from the Windows app store. Here is a [useful guide describing the process](https://www.thewindowsclub.com/run-bash-on-windows-10)

First time setup / Installation
===

Environment file
---
Ensure that you create a `.env` file by taking a copy of the `.env.example` file first. You should set as many variables as needed. Required variables will throw an exception on application start if not set. This step is important as it is how we set the database connection string and other important environment specific variables (environment specific means that the value changes between deployable instances, such as production, test & local). To understand the purpose of each of the variables, comments are included within `.env.example`.

As a minimum you will need to set the following:

```
    SITE_ENV="local" 
    SITE_FULL_URL="https://example.com" 
    SITE_DOMAIN_ONLY="example.com" 

    ROOT_FOLDER="public_html"
    THEME_SLUG="starter-theme"

    DB_NAME=""
    DB_USER="devuser"
    DB_PASSWORD="DeltaUniform6!"
    DB_HOST="localhost"
    DB_PREFIX="al4_"

    ACF_PRO_KEY="acf pro key"
```

`SITE_FULL_URL` can be left blank to use the URL set in the database, this is usually the URL for production and often the `.env.production` will be left blank. Otherwise, you should set this to your enviroment URL.


Composer, NPM & Gulp
---
Many assets are drawn in from Composer and NPM, with static assets being compiled using Gulp. To execute the commands to carry out these tasks you will need to be at the **root directory** of the website in your command window (i.e. where the gulpfile.js, composer.json & package.json files are).

#### Composer
You can find full details on [Composer](https://getcomposer.org/) online. For this project we maintain all public plugins within the `composer.json` file. To install/update plugins for the project run `composer i` or `composer u`. New plugins can be added using `composer require wpackagist-plugin/<plugin-slug>`.

#### NPM
You can find full details on [NPM](https://www.npmjs.com/) online. For this project we draw in node modules for asset library like Bootstrap. All libraries are stored within the `package.json` file. To install/update libraries for the project run `npm i`. We also use NPM to run the scripts referred to later in this read me.

#### Gulp
You can find full details on [Gulp](https://gulpjs.com/) online. For this project we compile assets and move development files into the distribution folder using Gulp. For example, Gulp is responsible for compiling and optimising Sass and JS. It also draws in NPM libraries and includes them in the `static` folder within our theme.

In the development environment there are three Gulp tasks:

1. Default
1. Build
1. Production

Default triggers the **watch** command and should be run during development. When you make changes it will compile and move assets into the distribution folder immediately ensuring the website shows your developments.

Build triggers all commands as non-optimised. This means it will compile Sass, JS, move development assets, move PHP assets and so on, to the distribution folder. Unlike the watch command this does everything, not just individual files when they are updated.

Production triggers all commands as optimised. This is the same as Build, but also minifies assests and purges unused code to produce the assets that would be used on the production environment. This process is a little slower, but is best used when determining an issue exclusive to the production environment within an optimised file.


Quick setup
---

#### Install WordPress & Plugins

1. Ensure that you have completed the Setup Environment Variables instructions above and set DB, ADMIN, SITE variables (even if the DB doesn't exist yet).
1. Run `npm run first-time-setup`. This install composer and npm, then run the gulp build task. This should also create the "public_html" folder with a working version of the website.
1. Run `npm run create-wp-database`. This will create the database and run through the installation.
1. For new projects only, you can get a new Timber starter theme by running the `npm run install-theme-files` command. This will download the latest starter theme from https://github.com/timber/starter-theme

**Note** Presently, the above scripts are PowerShell scripts, which work on Windows. But, there are Shell script alternatives within the `installation` folder of the same name that can be used on Linux based machines. 

#### Pull Live Database (Existing installation)

1. By Default, the *WP Sync DB* plugin is included in the composer configuration and should be installed and active on all instances of the project.
1. Copy and paste the "connection info" string from an existing site into the `.env` to set the `WPSDB_CONNECTION_INFO` variable, replacing the line break with `\n`.
1. Run `npm run import-live-database`. This will activate the WP Sync plugins and initiate a pull of the database targetted in the connection info.
1. This step can be repeated to refresh development environment databases from production.
1. Do **NOT** use this script to pull or push to the production environment.

Manual setup
---

#### Install WordPress & Plugins

1. Create a local MySQL database and put the connection details into the `.env` file as described in the Setup Environment Variables instructions above. Also set ADMIN and SITE variables if possible.
1. Run `npm run first-time-setup`. This install composer and npm, then run the gulp build task. This should also create the "public_html" folder with a working version of the website.
1. Visit the site and you should get the WordPress installation wizard. Step through this process to completion noting the password. Everything you do here will be overwritten when we pull the live DB.
1. Login to the WordPress area using the password you set at installation and follow the instructions below to pull a copy of the live database.

#### Pull Live Database (Existing installation)

1. Install DB Sync on the production system and activate it (should already be done for existing setups).
1. On your local system login to WordPress and manually copy over the "connection info" from the production system into your local DB Sync. Press the migrate button to start the database migration.

New projects
===

New builds
---
When starting development for brand new projects, I would advise you to create a WordPress installation on the live hosting environment where the site will be eventually hosted. Most hosts will allow you to do a "one-click WordPress installation", do this if available or manually install WordPress if not. Now you can setup the database variables in the `.env.production` file from the details of the `wp-config.php` file created by your installation. 

Next, follow the steps from the [First time setup / Installation](markdown-header-first-time-setup--installation) instructions to create your WordPress installation and database, then pull a copy of the live database by manually installing DB Sync to get the "connection info" of the production system. This can then be put into your local `.env` or `.env.development` file. This makes it possible to in order to pull the live database in future using scripts. 

Converting existing project to this structure
---
It is easy to apply this structure to existing projects with the following steps:

1. Ensure plugins are in the `composer.json` rather than included directly into source control. If the plugin folders are already included in the project source files you must search for their ID names in https://wpackagist.org/ to see if a repository exists and then include it. Where a plugin is not available (usually something custom or a paid for "pro" plugin), put this into the `wordpress/plugins` folder.
1. Move any bespoke or premium themes into the `wordpress/themes` folder.
1. Ensure Composer has installed and updated all assests, then run the build task from Gulp to generate the website within the "public_html" folder.

Development
===
Development assets are stored in the one of the folders `development` or `wordpress` and scripts are stored in the `installation` folder. There is then a [distribution folder](#markdown-header-distribution-folder---public_html) which is "built" by running the Composer, NPM & Gulp tasks. These tasks take the assests from the development folders and optimisers and/or copies the files to the distribution folder. This allows us to seperate our concerns and also reduce the need to store files that can either be compiled or drawn in from an external library (like Composer).

When you start development run the `gulp default` command and it will automatically copy over assets from the `themes`, `plugins` & `development` folders into the distribution folder. It will also start a **watch** command to automatically copy over updated development assets as they change.

Folder structure
---

#### WordPress folder

All theme and plugin development assests belong in the relevant `wordpress` sub-folder. When creating a new theme, place the folder into the `themes` folder and create assets as you normally would inside the `/wp-content/themes` folder. The same rule applies for plugin development and bespoke plugins not available via Composer, place these within the `plugins` folder.

#### Client-side development assets

All client-side development assets belong in the `development` folder in the root of the project. These include css, scss, javascript, fonts, images, etc as well as any starter theme assets. The provided `gulpfile.js` is setup to move assets from this location into the `theme/static` folder. Depending on the environment assets will be either optimised or non-optimised. The principle to this requirement is to ensure that all assets that are used on live are **compiled & optimised**. This is done by bundling, purging & minifying assets.

#### Distribution folder - "public_html"
This structure uses a distribution folder whos purpose is to contain "built assets" only. To start with this folder is empty, but is populated when the Composer, NPM & Gulp tasks are executed. By default the distribution folder is called "pubic_html", because this matches the root folder name for Site Ground and hence makes it easier to deploy. To suit alternative hosting environments this folder may be renamed to suit. For example, with Plesk hosting environments the folder name "httpdocs" maybe more suitable, so to use this instead rename the folder and also the references in the `.gitignore`, `.deployignore` & `composer.json`.

Maintenance
---

#### WordPress & Plugin Updates
The **production** version of the website will naturally update both the WordPress version and the installed Plugins whenever an update is available, using either auto-updates or WPMain. In order to match the source code and development environments in this repository to what is on production, it is **important** to run `composer u` to update the versions stored in the `composer.json` file whenever you start working on the project. Also, when introducing new plugins to the system it should be done using `composer require` so that it is appropriately stored in source control.

Deployment
===
This repository is setup to work with DeployHQ to automatically deploy changes after a `push` action is performed. The Production version of this system is assumed to be hosted on Site Ground. The only relevance of this is that we don't include plugins in our initial Composer setup for the purpose of backups or other plugins who's features would otherwise be redundant due to Site Ground's out-of-the-box features.

#### First time (Manual)
When deploying to an existing hosting environment using DeployHQ that already contains the source files, then I would recommend doing the following before your first deployment:

1. The new style `wp-config.php` file is often uploaded first. This requires the `.env` file and the `vendor` folder. So, using FTP, manually upload these two assets before a deployment in order to avoid downtime.
1. The rest of the process at this point will be largely a case of overwritting files already on the server and shouldn't cause too much trouble.
1. Check and ensure plugins are all up to date as sometimes a Composer version of a plugin can be a version or two behind (not sure why this is)


