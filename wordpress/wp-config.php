<?php
/**
 * Include Dotenv library to pull config options from .env file.
 */
if(file_exists(__DIR__ . '/vendor/autoload.php')) {
	require_once __DIR__ . '/vendor/autoload.php';
	$dotenv = Dotenv\Dotenv::createUnsafeImmutable(dirname(__DIR__, 1));
	$dotenv->load();
}
else {
	throw new Exception("Could not find autoload.php");
}

$dotenv->required(['DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASSWORD', 'DEBUG_MODE', 'THEME_SLUG', 'SITE_ENV']);

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

if (getenv('SITE_FULL_URL')) {
	define('WP_HOME', getenv('SITE_FULL_URL'));
	define('WP_SITEURL', getenv('SITE_FULL_URL'));
}

// ** MySQL settings - You can get this info from your web host ** //
/** NOTE: This is not a mistake. SiteGround will not be able to "manage" 
 * the WordPress installation unless it can match the pattern of the 
 * second block (possibly Regex), where strings are used instead of 
 * variables. The "define" method should set a constant which cannot 
 * be updated, so we use the true environment variables first. We then 
 * match the validation pattern with any old string to satisfy Site 
 * Ground. We don't need to redefine the true settings again as the 
 * constant shouldn't be overwritable at this stage, but we do this to 
 * be on the safe side incase this method changes in the future. 
 */
define( 'DB_NAME', getenv('DB_NAME') );
define( 'DB_USER', getenv('DB_USER') );
define( 'DB_PASSWORD', getenv('DB_PASSWORD'));
define( 'DB_HOST', getenv('DB_HOST') );

define( 'DB_NAME', 'sg_validation' );
define( 'DB_USER', 'sg_validation' );
define( 'DB_PASSWORD', 'sg_validation' );
define( 'DB_HOST', 'sg_validation' );

define( 'DB_NAME', getenv('DB_NAME') );
define( 'DB_USER', getenv('DB_USER') );
define( 'DB_PASSWORD', getenv('DB_PASSWORD'));
define( 'DB_HOST', getenv('DB_HOST') );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hN2 `|%;|RJc!Z)} ;{f.)&+Fu#XVv.X/X6NF,G:9|#5PAEijdK_Hx51z%N%M]zQ');
define('SECURE_AUTH_KEY',  'XzF@0Tg|Sn|ptJu}+G{EUw Ip s-^bnC,~K;BoIikBZR,64 B<#7~S?Je(>oXJ%3');
define('LOGGED_IN_KEY',    '3}0)MJ<trC T~v$edj/x5cnC6*|?hr,yNzp3UBeQxGM++#ksA:Jg;K0=664H9rSz');
define('NONCE_KEY',        'VP%q8[V$h,V:-.Wt`<:!4Hh Snqy? m1K,F&:MR;w2NTVd6[nAsV|cj+y1{A,Iz>');
define('AUTH_SALT',        ';{XsZ]m1dXP5k9Ba~B|qEddO;f|_v.KkS|Ua* 3jrPX9i*GcAe7b_;qsYP_V<^$K');
define('SECURE_AUTH_SALT', '=*n@1_hUrD#nF+_dcS BT DkOg+tn7.lsG;gXIMWp;&f{%#kgT7o2E<[^P--`Tg>');
define('LOGGED_IN_SALT',   '[2x5Y()3B`S-n{6h/:C9+%CL8SscH-jourB >:@|]4e6/5H+6RwPk1YSld;JvGy:');
define('NONCE_SALT',       '<^UP6]foJ<vG6sKGL]GF0y8X~@-r&r-mK%{w;FkEU:Lz0UdSH,dSlp,i@1us-L-g');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';
$table_prefix = getenv('DB_PREFIX');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', (strtolower(getenv('DEBUG_MODE')) === "true"));

/**
 * For Multisite uncomment below lines
 * define('MULTISITE', true);
 * define('SUBDOMAIN_INSTALL', false);
 * define('DOMAIN_CURRENT_SITE', getenv('SITE_DOMAIN'));
 * define('PATH_CURRENT_SITE', '/');
 * define('SITE_ID_CURRENT_SITE', 1);
 * define('BLOG_ID_CURRENT_SITE', 1);
 */

/* That's all, stop editing! Happy publishing. */

if (getenv('ACF_PRO_KEY')) {
	// Defined ACF key programatically 
	define('ACF_PRO_LICENSE', getenv('ACF_PRO_KEY'));
}

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
@include_once('/var/lib/sec/wp-settings.php'); // Added by SiteGround WordPress management system